use "collections"
use "random"
use "time"

actor Main
  var env: Env
  var tree: BinaryTree

  // keep track of different interactions with different trees
  var state: MapIs[BinaryTree, U8] = MapIs[BinaryTree, U8]

  let state_names: {(U8): String} = {(state) =>
    match state
    | 0 => "traversed tree for the first time"
    | 1 => "deleted all occurrences of a number in tree"
    else
      "oops"
    end}


  new create(env': Env) =>
    this.env = env'
    tree = BinaryTree(this, 42)
    try state.insert(tree, 0)? end
    // tree.insert(13)
    // tree.insert(666)
    // tree.insert(13)
    // tree.insert(7)
    let rng = Rand
    for i in Range(0, 10) do
      let new_val: Leaf = rng.int(100).i64()
      tree.insert(new_val)
    end
    tree.begin_traversal()

  be printout(s: String) =>
    env.out.print(s)

  be return_traversal(it: Iterator[Leaf] iso) =>
    let bin_st: U8 = try state(tree)? else -1 end
    env.out.print("\n[state "+bin_st.string()+" (" + state_names(bin_st)+")]")
    // env.out.print("[" + "; ".join(consume it) + "]\n")
    let rng: Rand = Rand(Time.nanos(), Time.nanos())
    var to_delete: Leaf = -1
    for elem in consume it do
      env.out.print(" - " + elem.string())
      if rng.int(100) < 20 then to_delete = elem end
    end

    match bin_st
    | 0 => tree.delete_all(to_delete)
    end
    state(tree) = bin_st + 1




type Leaf is I64
type MaybeBinaryTree is (BinaryTree | None)

primitive JustTraverse fun string(): String => "JustTraverse"
primitive ForDeletion  fun string(): String => "ForDeletion"

actor BinaryTree
  var value: Leaf
  var left: MaybeBinaryTree = None
  var right: MaybeBinaryTree = None
  var main: Main
  var traversal_state: (U8, BinaryTree) = (0, this)
  var traversal_reason: (JustTraverse | (ForDeletion, Leaf) | None) = None

  new create(main': Main, initial_value: Leaf) =>
    main = main'
    value = initial_value
    main.printout("Node created: " + initial_value.string())

  // Insertion is simple and idiomatic
  be insert(num: Leaf) =>
    if num < value then
      match left
      | None => left = BinaryTree(main, num)
      | let leftTree: BinaryTree => leftTree.insert(num)
      end
    else
      match right
      | None => right = BinaryTree(main, num)
      | let rightTree: BinaryTree => rightTree.insert(num)
      end
    end


  // traversal behaviours
  // TBD: Use lambdas/Promises and try to make a better use of the Pony type
  //      system in order to reduce these 3 behaviours to a single one

  be begin_traversal() =>
    log("starting traversal")
    traversal_reason = JustTraverse
    let traversed_vals: Array[Leaf] iso = []
    traverse(this, consume traversed_vals)


  be traverse(who_asked: BinaryTree, traversed_vals: Array[Leaf] iso) =>
    traversal_state = (1, who_asked)
    match left
    | let leftSubTree: BinaryTree =>
      // Se left for uma BinaryTree, envia traversed_vals a ela para depois ela
      // me a devolver atravavés do meu behaviour return_traversal
      log("leftSubTree.traverse")
      leftSubTree.traverse(this, consume traversed_vals)
    | None =>
      // left é None. vou enviar traversed_vals para mim próprio sem alterações
      log("this.return_traversal (left)")
      this.return_traversal(consume traversed_vals)
    end
    // traversed_vals.push(value)
    // who_asked.return_traversal(consume traversed_vals)


  be return_traversal(traversed_vals: Array[Leaf] iso) =>
    match traversal_state
    | (1, let who_asked: BinaryTree) =>
      // Acabei de visitar o meu ramo esquerdo
      traversal_state = (2, who_asked)
      traversed_vals.push(value)
      match right
      | let rightSubTree: BinaryTree =>
        // Se right for uma BinaryTree, envia traversed_vals a ela para depois
        // ela me a devolver atravavés do meu behaviour return_traversal
        log("rightSubTree.traverse")
        rightSubTree.traverse(this, consume traversed_vals)
      | None => 
        // right é None. vou enviar traversed_vals para mim próprio outra vez
        log("this.return_traversal (right)")
        this.return_traversal(consume traversed_vals)
      end
    | (2, let who_asked: BinaryTree) =>
      // Acabei de visitar o meu ramo direito
      // Vou devolver a lista de nós visitados a quem me a pediu
      if who_asked is this then
        log("end of traversal")
        match traversal_reason
        | JustTraverse =>
          log("giving iterator to main actor")
          main.return_traversal(recover (consume traversed_vals).values() end)
        | (ForDeletion, let to_delete: Leaf) =>
          log("Now let's delete_all " + to_delete.string() + "...")
          chop_and_regrow(consume traversed_vals, to_delete)
        end
      else
        log("who_asked.return_traversal")
        who_asked.return_traversal(consume traversed_vals)
      end
    else
      log("wat")
    end


  // Deletion behaviours

  be delete_all(num: Leaf) =>
    log("you asked me to delete_all " + num.string())
    let traversed_vals: Array[Leaf] iso = []
    traversal_reason = (ForDeletion, num) // para fazer chop_and_regrow(_, num)
    traverse(this, consume traversed_vals)

  be chop_and_regrow(traversed_vals: Array[Leaf] iso, to_delete: Leaf) =>
    var first_elem: Bool = true
    let filtered_vals: Array[Leaf] iso = recover Array[Leaf] end

    for elem in (consume traversed_vals).values() do
      if elem != to_delete then
        filtered_vals.push(elem)
        if first_elem then
          // Chop myself
          log("chop - " + elem.string())
          value = elem
          left = None
          right = None
          first_elem = false
        else
          // Regrow, but without the values that we wanted to delete
          log("regrow - " + elem.string())
          insert(elem)
        end
      else
        log("discard - " + elem.string())
      end
    end
    
    var it: Iterator[Leaf] iso = recover (consume filtered_vals).values() end
    main.return_traversal(consume it)
    

  // other useful behaviours

  be log(msg: String) =>
    """
    for testing purposes (comment/uncomment for less/more verbose output)
    """
    // main.printout("  [" + value.string() + "]\t-> " + msg)


// let copy: Array[Leaf] iso = recover (consume traversed_vals).clone() end
//       log("" + ", ".join((consume copy).values()))