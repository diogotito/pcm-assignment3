class Counter
  var count: U64

  new create(start: U64) =>
    count = start

  fun ref inc() =>
    count = count + 1

  fun ref dec() =>
    count = count - 1

  fun get(): U64 =>
    count


actor VeryMain
  new create(env: Env) =>
    let c = Counter(42)
    c.inc()
    c.dec()
    env.out.print(id[U64](c.get()).string())

  fun id[A: Any](a: A):A  => a
