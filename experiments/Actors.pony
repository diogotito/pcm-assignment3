class Wombat is Stringable
  let name: String
  var _hunger_level: U64

  new create(name': String) =>
    name = name'
    _hunger_level = 0

  new hungry(name': String, hunger': U64) =>
    name = name'
    _hunger_level = hunger'

  fun string(): String iso^ =>
    ("Wombat " + name).clone()


actor ActorsMain
  new create(env: Env) =>
    let w: Wombat = Wombat("Winston")
    printout(env.out, w)
    env.out.>print(if true then
      nope(env.out)
      "Hello"
    else
      "Goodbye"
    end + ", World!").print("yeah")

  var numeros: Array[String] val = ["um"; "dois"; "tres"]
  env.out.print("[" + "; ".join(numeros.values()) + "]")

  env.out.print(try
    numeros(1)?.string()
  else
    "oops"
  end)

  fun nope(out: OutStream): None => out.print("nothing...")

  fun printout(out: OutStream, obj: Stringable) =>
    out.print("=> " + obj.string())
