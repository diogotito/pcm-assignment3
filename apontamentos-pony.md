# Apontamentos dispersos

## Actores

instâncias de Actors são criadas e retornadas imediatamente, e só depois é que o construtor é executado assincronamente

## Sistema de tipos

Pony tem structural (via interfaces) typing e ____ typing (via traits) em simultâneo!

type aliases com `type <name> is <type expression>`

em type expressions posso fazer coisas bacanas como `&`, `|` e tuplos (`(,)`)


## Capabilites

Ui... vou-me lixar tanto.

### Soluções penso-rápido encontradas

`val` -> `iso^` em Strings:

    string.clone()

ou usar `recover` como deve ser?
parece que a stdlib gosta muito de `iso^`s

- - -



## Controlo de fluxo

`try-catch-finally` --> `try-else-then` (o then também é **sempre** chamado,
mesmo quando há um early `return` dentro do `try`)

As únicas construções da linguagem que atiram erros são o `error` e o `as`

é `if`, `else`, `elseif` e `end`

Posso por `else` no `for`, no `while` e no `repeat until` (e também no `match`)

	In general, Pony else blocks provide a value when the expression they are
	attached to doesn't

`break` pode levar uma expressão para dar o valor de retorno ao loop

senão --> `None`

(não confundir com o else do for do Python)

## Funções

Na matemática, funções parciais são funções que não têm um resultado definido
para todos os seus inputs possíveis

Não posso mexer nos parâmetros dentro das funções como noutras linguagens

Method chaining com `.>`!

## Variáveis

`var` -> *var*iável ;)
`let` -> constante, como em Haskell (não como em JS)

Números:
```
let my_explicit_unsigned: U32 = 42_000
let my_constructor_unsigned = U8(1)
let my_constructor_float = F64(1.234)
```

posso fazer conversões com `i64()`, `isize()`, `ilong()`, `u64()`, `f64()`, etc.

para além de operadores aritméticos normais, tenho operadores unsafe e partial
and checked arithmetic

## Rubyismos

parece que muitas construções no Pony com ar de blocos são expressões e não "statements", e o seu resultado é o da última expressão lá dentro. Isto acontece em:

  * funções
  * métodos
  * blocos `with`



# outros apontamentos

local alias: no mesmo actor
global alias: de outro actor
