use "tree_callback"
use "collections"
use "promises"
use "random"
use "time"


// Main actor to test the above tree implementation
actor Main
  new create(env: Env) =>
    let tree: TreeNode = TreeNode(42)
    let rng = Rand(Time.nanos(), Time.nanos())
    tree.insert(666); tree.insert(666); tree.insert(666)
    for i in Range(0, 10) do
      let new_val: Leaf = rng.int(100).i64()
      tree.insert(new_val)
    end

    // Check if certain values are in a binary tree
    let vals: Array[Leaf] = [7; 8; 91; 11; 12; 13; 13; 42]
    for n in vals.values() do
      tree.find_node(n, {(result: BinaryTree)(env,n) =>
        match result
        | let node_found: TreeNode => node_found.access({(node: TreeNode ref)=>
            env.out.print(n.string() + " in tree? Yes!! " + node.string())
          })
        | None => env.out.print(n.string() + " in tree? nope.")
        end
      })
    end

    tree.traverse(recover Array[Leaf] end, {(values: Array[Leaf] iso)(env) =>
      env.out.print("  [1]: " + "; ".join((consume values).values()))
      let p: Promise[None] = Promise[None]
      let tree' = tree
      p.next[None]({(n)(env, tree') =>
        env.out.print("A deletion successfully occurred :D")
        tree.traverse(recover Array[Leaf] end, {(values': Array[Leaf] iso) =>
          env.out.print("  [2]: " + "; ".join((consume values').values()))
        })
      })
      tree.delete(666, p, env.out~print())
    })
