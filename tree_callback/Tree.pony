use "promises"
use "random"

type Leaf is I64
type BinaryTree is (TreeNode | None)

actor TreeNode is Stringable
  var value: Leaf
  var parent: BinaryTree = None
  var left: BinaryTree = None
  var right: BinaryTree = None

  new _with_parent(initial_value: Leaf, parent': BinaryTree) =>
    value = initial_value
    parent = parent'

  // default constructor
  new create(initial_value: Leaf) =>
    value = initial_value
    parent = None

  fun string(): String iso^ =>
    value.string()

  // The "access pattern" (https://patterns.ponylang.io/async/access.html) to
  // simplify code in places where it is more convenient to access this actor's
  // fields in a synchronous way
  be access(fn: {(TreeNode ref)} val) =>
    fn(this)


  // -------------------------------------------------------------------------
  // Insert operation
  // -------------------------------------------------------------------------

  be insert(num: Leaf) =>
    if num < value then
      match left
      | let leftTree: TreeNode => leftTree.insert(num)
      | None => left = TreeNode._with_parent(num, this)
      end
    else
      match right
      | let rightTree: TreeNode => rightTree.insert(num)
      | None => right = TreeNode._with_parent(num, this)
      end
    end

  
  // -------------------------------------------------------------------------
  // Contains operation
  // -------------------------------------------------------------------------

  be find_node(to_search: Leaf, callback: {(BinaryTree)} val) =>
    if value == to_search then
      callback(this) // found it
    else
      // Put the logic of searching in the right branch after searching in the
      // left branch in a lambda
      let search_in_right: {(BinaryTree)} val = {(found_in_left) =>
        match found_in_left
        | let node_found: TreeNode => callback(node_found)
        | None => match right
          | let right': TreeNode => right'.find_node(to_search, callback)
          | None => callback(None)
          end
        end
      }

      // Search left if we do have a left branch. Then search right.
      match left
      | let left': TreeNode => left'.find_node(to_search, search_in_right)
      | None => search_in_right(None)
      end
    end


  // -------------------------------------------------------------------------
  // Delete operation
  // -------------------------------------------------------------------------

  be traverse(values: Array[Leaf] iso, callback: {(Array[Leaf] iso)} val) =>
    let traverse_right: {(Array[Leaf] iso)} val = {(values': Array[Leaf] iso) =>
      values'.push(value)
      match right
      | let right': TreeNode => right'.traverse(consume values', callback)
      | None => callback(consume values')
      end
    }

    match left
    | let left': TreeNode => left'.traverse(consume values, traverse_right)
    | None => traverse_right(consume values)
    end

  be delete(value_to_delete: Leaf, p: Promise[None], print: {(String)} val) =>
    traverse(recover Array[Leaf] end,
             {(all_values: Array[Leaf] iso)
              // `this` inside lambda is bound to the lambda's object
              (self: TreeNode tag = this, p, print) =>

      (let head: Leaf val, let rest: Array[Leaf] val) = recover
        try all_values.delete(all_values.find(value_to_delete)?)? end
        ((try all_values.shift()? else -1 end), consume all_values)
      end

      self.access({(self': TreeNode ref) =>
        self'.value = head
        self'.left = None
        self'.right = None

        for v in rest.values() do
          self'.insert(v)
        end

        p(None)
      })
    })
