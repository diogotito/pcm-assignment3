i3-msg "workspace --no-auto-back-and-forth 5:Trab"
sleep 0.1s
i3-msg append_layout "$(pwd)/i3-workspace.json"
sleep 0.1s
i3-msg exec subl
i3-msg exec "xfce4-terminal --working-directory=$(pwd)"

terminal() {
    title=$1
    [ $# -gt 1 ] && shift
    xfce4-terminal --working-directory=$(pwd) -T "$title" -e "bash -c \"$*\""
    i3-msg border normal >/dev/null
    i3-msg focus up >/dev/null
}

i3-msg split v >/dev/null
terminal "Compiler messages" "while true; do find *.pony | entr -d bash -c 'tput reset && ponyc'; done"
terminal "Output" "entr bash -c 'tput reset && ./$(basename $(pwd))' <<< $(basename $(pwd))"

